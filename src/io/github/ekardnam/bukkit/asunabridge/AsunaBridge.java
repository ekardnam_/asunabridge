package io.github.ekardnam.bukkit.asunabridge;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.ekardnam.telegram.asunabridge.PlayersCommand;
import io.github.ekardnam.telegram.asunabridge.SendCommand;
import me.palazzomichi.telegram.telejam.Bot;
import me.palazzomichi.telegram.telejam.UpdateChecker;
import me.palazzomichi.telegram.telejam.util.text.Text;

public final class AsunaBridge extends JavaPlugin {
	
	private static final String BOT_TOKEN = "472731154:AAG5msNxi6z-650oBVk9zEBsFhY5Gubnhks";
	private static final long CHAT_ID = 122074981;	
	
	private static AsunaBridge instance;
	
	public static AsunaBridge getInstance() {
		return instance;
	}
	
	public static void sendTgMessage(String message) {
		try {
			getInstance().getBot().sendMessage(getInstance().getBot().getChat(CHAT_ID), new Text(message), null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Bot bot;
	
	private UpdateChecker checker;
	
	//helper attributes
	private boolean loaded = true;
	
	@Override
	public void onLoad() {
		instance = this;
		try {
			bot = new Bot(BOT_TOKEN);
		} catch (IOException e) {
			loaded = false;
			e.printStackTrace();
		}
		checker = new UpdateChecker(bot);
		
		checker.getUpdateHandlers().add(new PlayersCommand());
		checker.getUpdateHandlers().add(new SendCommand());
	}
	
	@Override
	public void onEnable() {
		if (!loaded) {
			Bukkit.getServer().getPluginManager().disablePlugin(this);
			return;
		}
		Bukkit.getScheduler().runTaskTimer(this, checker, 0, 500);
		Bukkit.getServer().getPluginManager().registerEvents(new EventListener(), this);
	}
	
	public Bot getBot() {
		return bot;
	}

}
