package io.github.ekardnam.bukkit.asunabridge;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class EventListener implements Listener {
	
	@EventHandler
	public void chatEvent(AsyncPlayerChatEvent e) {
		if (e.getMessage().startsWith("!")) {
			AsunaBridge.sendTgMessage(e.getMessage().substring(1));
			e.setMessage(e.getMessage().substring(1));
		}
	}

}
