package io.github.ekardnam.telegram.asunabridge;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import io.github.ekardnam.bukkit.asunabridge.AsunaBridge;
import me.palazzomichi.telegram.telejam.objects.messages.TextMessage;
import me.palazzomichi.telegram.telejam.util.handlers.CommandExecutor;
import me.palazzomichi.telegram.telejam.util.text.Text;

public class PlayersCommand extends CommandExecutor {

	public PlayersCommand() {
		super(AsunaBridge.getInstance().getBot(), "players");
	}

	@Override
	public void execute(String cmd, String[] args, TextMessage message) throws Throwable {
		StringBuffer reply = new StringBuffer();
		if (Bukkit.getServer().getOnlinePlayers().size() > 0) {
			reply.append("Ciao ^.^, online ci sono: ");
			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
				reply.append(p.getDisplayName() + " ");
			}
		} else {
			reply.append("Online non c'è nessuno (-.-;)");
		}
		bot.sendMessage(message.getChat(), new Text(reply.toString()), null);
	}

}
