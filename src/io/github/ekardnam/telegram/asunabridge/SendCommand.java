package io.github.ekardnam.telegram.asunabridge;

import org.bukkit.Bukkit;

import io.github.ekardnam.bukkit.asunabridge.AsunaBridge;
import me.palazzomichi.telegram.telejam.objects.messages.TextMessage;
import me.palazzomichi.telegram.telejam.util.handlers.CommandExecutor;

public class SendCommand extends CommandExecutor {

	public SendCommand() {
		super(AsunaBridge.getInstance().getBot(), "send");
	}

	@Override
	public void execute(String cmd, String[] args, TextMessage message) throws Throwable {
		StringBuffer sent = new StringBuffer();
		for (String s : args) {
			sent.append(s + " ");
		}
		Bukkit.broadcastMessage("From " + message.getSender().getName() + ": " + sent);
	}

}
